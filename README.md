# born/died/swiss/2011
## a Make.Opendata.ch project

Animated visualization of Swiss birth and death stats using Web graphics.

This is a http://make.opendata.ch project created in Geneva, Switzerland on 28.09.2012. For details and data sources see:

http://make.opendata.ch/doku.php?id=project:born-died-ch

Inspired by:
------------
* http://www.breathingearth.net/
* https://www.cia.gov/library/publications/the-world-factbook/
* http://www.smartjava.org/content/render-geographic-information-3d-threejs-and-d3js

Made with:
----------
* https://github.com/mrdoob/three.js/
* http://sole.github.com/tween.js/examples/03_graphs.html
* https://github.com/asutherland/d3-threeD

<3.	
